package net.gitlab.richthelord.BlockParty.Utils;

import net.gitlab.richthelord.BlockParty.Main;

import java.io.File;
import java.util.logging.Level;

@SuppressWarnings("deprecation")
public class SchmaticUtils {
    public static void loadModels() {
        File folder = new File("plugins/BlockParty/schematics");
        File[] list = folder.listFiles();

        for (File aList : list) {
            if (aList.isFile()) {
                String Fl = aList.getName().split("\\.")[0];

                //Main.getFloormodels().add(loadSchem(Fl));
                Main.getFloorfiles().add(aList);
                Main.getPlugin().getLogger().log(Level.INFO, Main.PREFIX_RAW + "Cargando: " + Fl, aList);
            }
        }

    }

    //EditSession es = new EditSession(bw, Integer.MAX_VALUE);
}
