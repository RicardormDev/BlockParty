package net.gitlab.richthelord.BlockParty.Utils;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;

public class FireworksUtil{
        @SuppressWarnings("static-access")
		public static void shootFireworks(Player player) {
            Firework fw = (Firework) player.getWorld().spawn(
            player.getLocation(), Firework.class);
            FireworkMeta fm = fw.getFireworkMeta();
            Random r = new Random();
            int fType = r.nextInt(5) + 1;
            FireworkEffect.Type type = null;
            switch (fType) {
                default:
                case 1:
                    type = type.BALL;
                    break;
                case 2:
                    type = type.BALL_LARGE;
                    break;
                case 3:
                    type = type.BURST;
                    break;
                case 5:
                    type = type.STAR;
                    break;
                case 4:
                    type = type.CREEPER;
                    break;
            }
            int c1i = r.nextInt(16) + 1;
            int c2i = r.nextInt(16) + 1;
            Color c1 = getColour(c1i);
            Color c2 = getColour(c2i);
            FireworkEffect effect = FireworkEffect.builder().flicker(r.nextBoolean()).
                    withColor(c1).withFade(c2).with(type).trail(r.nextBoolean()).build();
            fm.addEffect(effect);
            int power = r.nextInt(2) + 1;
            fm.setPower(power);
            fw.setFireworkMeta(fm);

    }
    
    @SuppressWarnings("static-access")
	public static void shootFireworksAuto(Location loc) {
            Firework fw = (Firework) loc.getWorld().spawn(loc.clone(), Firework.class);
            FireworkMeta fm = fw.getFireworkMeta();
            Random r = new Random();
            int fType = r.nextInt(5) + 1;
            FireworkEffect.Type type = null;
            switch (fType) {
                default:
                case 1:
                    type = type.BALL;
                    break;
                case 2:
                    type = type.BALL_LARGE;
                    break;
                case 3:
                    type = type.BURST;
                    break;
                case 5:
                    type = type.STAR;
                    break;
                case 4:
                    type = type.CREEPER;
                    break;
            }
            int c1i = r.nextInt(16) + 1;
            int c2i = r.nextInt(16) + 1;
            Color c1 = getColour(c1i);
            Color c2 = getColour(c2i);
            FireworkEffect effect = FireworkEffect.builder().flicker(r.nextBoolean()).
                    withColor(c1).withFade(c2).with(type).trail(r.nextBoolean()).build();
            fm.addEffect(effect);
            int power = r.nextInt(2) + 1;
            fm.setPower(power);
            fw.setFireworkMeta(fm);

    }
      @SuppressWarnings("static-access")
	public static void shootFireworksAll() {
          for (Player player : Bukkit.getOnlinePlayers()){
            Firework fw = (Firework) player.getWorld().spawn(
            player.getLocation(), Firework.class);
            FireworkMeta fm = fw.getFireworkMeta();
            Random r = new Random();
            int fType = r.nextInt(5) + 1;
            FireworkEffect.Type type = null;
            switch (fType) {
                default:
                case 1:
                    type = type.BALL;
                    break;
                case 2:
                    type = type.BALL_LARGE;
                    break;
                case 3:
                    type = type.BURST;
                    break;
                case 5:
                    type = type.STAR;
                    break;
                case 4:
                    type = type.CREEPER;
                    break;
            }
            int c1i = r.nextInt(16) + 1;
            int c2i = r.nextInt(16) + 1;
            Color c1 = getColour(c1i);
            Color c2 = getColour(c2i);
            FireworkEffect effect = FireworkEffect.builder().flicker(r.nextBoolean()).
                    withColor(c1).withFade(c2).with(type).trail(r.nextBoolean()).build();
            fm.addEffect(effect);
            int power = r.nextInt(2) + 1;
            fm.setPower(power);
            fw.setFireworkMeta(fm);
          }
    }

      private static Color getColour(int c) {
          switch (c) {
              default:
              case 1:
                  return Color.AQUA;
              case 2:
                  return Color.BLACK;
              case 3:
                  return Color.BLUE;
              case 4:
                  return Color.FUCHSIA;
              case 5:
                  return Color.GRAY;
              case 6:
                  return Color.GREEN;
              case 7:
                  return Color.LIME;
              case 8:
                  return Color.MAROON;
              case 9:
                  return Color.NAVY;
              case 10:
                  return Color.OLIVE;
              case 11:
                  return Color.ORANGE;
              case 12:
                  return Color.PURPLE;
              case 13:
                  return Color.RED;
              case 14:
                  return Color.SILVER;
              case 15:
                  return Color.TEAL;
              case 16:
                  return Color.WHITE;
              case 17:
                  return Color.YELLOW;
          }
      }

}
