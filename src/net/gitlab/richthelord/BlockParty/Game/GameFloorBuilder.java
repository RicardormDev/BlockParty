package net.gitlab.richthelord.BlockParty.Game;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.blocks.BaseBlock;
import com.sk89q.worldedit.util.Countable;
import net.gitlab.richthelord.BlockParty.Main;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@SuppressWarnings( "deprecation" )
public class GameFloorBuilder {

    private Random random;

    private List<Location> blockLocations;
    //private CuboidClipboard blocks;
    private Byte safe;

    private int ID;


    public GameFloorBuilder(List<Location> blockLocations) {
        this.random = new Random();
        this.blockLocations = blockLocations;

        predictSafe();
    }

    public GameFloorBuilder(List<Location> blockLocations, CuboidClipboard cuboidClipboard) {
        this.random = new Random();
        this.blockLocations = blockLocations;
        //this.blocks = Main.getFloormodels().get(getRandomBlock(0, Main.getFloormodels().size()));

        predictSafe();
    }
/*
    public CuboidClipboard getBlocks() {
        return blocks;
    }
*/
    public List<Location> getBlockLocations() {
        return blockLocations;
    }

    public Byte getSafe() {
        return safe;
    }


    public Byte predictSafe() {
        List<Byte> found = new ArrayList<>();

        for(Location loc : blockLocations) {
            Block block = loc.getBlock();
            if(block.getType() == Material.STAINED_CLAY)
                found.add(block.getData());
        }

        int n = getRandomBlock(0, found.size());
        this.safe = found.get(n);
        return safe;
    }

    private int getRandomBlock(int min, int max) {
        return random.nextInt(max-min) + min;
    }

    public void setBlockLocations(List<Location> blockLocations) {
        this.blockLocations = blockLocations;
    }
/*
    public void setBlocks(CuboidClipboard blocks) {
        this.blocks = blocks;
    }
*/
    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }
/*
    public List<Integer> loadB() {
        for(Countable<BaseBlock> b : blocks.getBlockDistributionWithData()) {
            //if(b.getID().getType() == 159)
            try {
                System.out.println((byte)b.getID().getData());
            } catch (Exception e){
                e.printStackTrace();
            }
        }
        return null;
    }*/
}
