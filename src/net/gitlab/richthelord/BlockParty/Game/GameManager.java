package net.gitlab.richthelord.BlockParty.Game;

import net.gitlab.richthelord.BlockParty.Main;
import net.gitlab.richthelord.BlockParty.SettingsManager;
import org.bukkit.configuration.ConfigurationSection;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class GameManager {

    private static GameManager manager = new GameManager();
    public static GameManager getManager() {
        return manager;
    }

    private List<Game> games;
    public GameManager() {
        games = new ArrayList<>();
    }

    public void registerGame(Game game) {
        this.games.add(game);
    }

    public void unregisterGame(Game game) {
        this.games.remove(game);
    }

    public Game getGame(String name) {
        for(Game game : games) {
            if(game.getGameName().equals(name)) {
                return game;
            }
        }
        return null;
    }

    public void loadArenas() {

        if(SettingsManager.getArenas().get("Arenas") == null){
            return;
        }

        for(String gameName : SettingsManager.getArenas().<ConfigurationSection>get("Arenas").getKeys(false)) {
            Game game = new Game(gameName);
            registerGame(game);

            Main.getPlugin().getLogger().log(Level.INFO, Main.PREFIX_RAW + "Cargando: " + gameName, game);
        }
    }
}
