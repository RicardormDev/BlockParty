package net.gitlab.richthelord.BlockParty.Game;

import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

public class GameScoreboard {

    private Game game;
    private ScoreboardManager manager;

    private Scoreboard scoreboard;
    private Objective objective;

    GameScoreboard(Game game) {
        this.game = game;
        create();
    }

    public Scoreboard create () {
        manager = Bukkit.getScoreboardManager();
        scoreboard = manager.getNewScoreboard();
        objective = scoreboard.registerNewObjective("Objective", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName("§3§lBlock§6§lParty");

        Score score = objective.getScore(" ");
        score.setScore(7);

        Score score2 = objective.getScore("Jugadores: §3" + game.getPlayers().size() + "/" + game.getMaxPlayers());
        score2.setScore(6);

        Score score3 = objective.getScore("§1");
        score3.setScore(5);

        Score score4 = objective.getScore("Esperando...");
        score4.setScore(4);

        Score score6 = objective.getScore("§5");
        score6.setScore(2);

        Score score7 = objective.getScore("§ewww.volkmc.net");
        score7.setScore(1);

        return scoreboard;
    }

    public void update(){
        switch (game.getGameStage()) {
            case WAITING:
                updateWaiting();
                break;
            case STARTING:
                updateStarting();
                break;
            case STARTED:
                updateStarted();
                break;
        }
    }

    public void updateWaiting() {
        scoreboard = manager.getNewScoreboard();
        objective = scoreboard.registerNewObjective("Objective", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName("§3§lBlock§6§lParty");

        Score score = objective.getScore(" ");
        score.setScore(7);

        Score score2 = objective.getScore("Jugadores: §3" + game.getPlayers().size() + "/" + game.getMaxPlayers());
        score2.setScore(6);

        Score score3 = objective.getScore("§1");
        score3.setScore(5);

        Score score4 = objective.getScore("Esperando...");
        score4.setScore(4);

        Score score6 = objective.getScore("§5");
        score6.setScore(2);

        Score score7 = objective.getScore("§ewww.volkmc.net");
        score7.setScore(1);

        for(GamePlayer gamePlayer : game.getPlayers()) {
            gamePlayer.getPlayer().setScoreboard(scoreboard);
        }
    }

    public void updateStarting() {
        scoreboard = manager.getNewScoreboard();
        objective = scoreboard.registerNewObjective("Objective", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName("§3§lBlock§6§lParty");

        int i = 0;

        if(game.getGameCountdown() != null) {
            i = game.getGameCountdown().getCount();
        }

        Score score = objective.getScore(" ");
        score.setScore(7);

        Score score2 = objective.getScore("Jugadores: §3" + game.getPlayers().size() + "/" + game.getMaxPlayers());
        score2.setScore(6);

        Score score3 = objective.getScore("§1");
        score3.setScore(5);

        Score score4 = objective.getScore("§rStarting: §3" + i);
        score4.setScore(4);

        Score score6 = objective.getScore("§5");
        score6.setScore(2);

        Score score7 = objective.getScore("§ewww.volkmc.net");
        score7.setScore(1);

        for(GamePlayer gamePlayer : game.getPlayers()) {
            gamePlayer.getPlayer().setScoreboard(scoreboard);
        }
    }

    public void updateStarted() {
        scoreboard = manager.getNewScoreboard();
        objective = scoreboard.registerNewObjective("Objective", "dummy");
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        objective.setDisplayName("§3§lBlock§6§lParty");

        Score score = objective.getScore(" ");
        score.setScore(7);

        Score score2 = objective.getScore("Jugadores: §3" + game.getPlayers().size() + "/" + game.getMaxPlayers());
        score2.setScore(6);

        Score score3 = objective.getScore("§1");
        score3.setScore(5);


        Score s = objective.getScore("Ronda: §3" + game.getRound());
        s.setScore(4);

        Score s1 = objective.getScore("Nivel Velocidad: §3" + game.getSpeed());
        s1.setScore(3);

        Score score6 = objective.getScore("§5");
        score6.setScore(2);

        Score score7 = objective.getScore("§ewww.volkmc.net");
        score7.setScore(1);

        for(GamePlayer gamePlayer : game.getPlayers()) {
            gamePlayer.getPlayer().setScoreboard(scoreboard);
        }
    }

    public void removeScoreboard(GamePlayer gamePlayer) {
        gamePlayer.getPlayer().setScoreboard(manager.getNewScoreboard());
    }
}
