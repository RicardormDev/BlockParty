package net.gitlab.richthelord.BlockParty.Game;

import net.md_5.bungee.api.ChatColor;

public enum GameColors {
    BLANCO("Blanco", ChatColor.WHITE, 0),
    NAJARANJA("Naraja", ChatColor.GOLD, 1),
    MAGENTA("Magenta", ChatColor.DARK_PURPLE, 2),
    AZUL_CLARO("Azul Claro", ChatColor.BLUE, 3),
    AMARILLO("Amarillo", ChatColor.YELLOW, 4),
    LIMA("Lima", ChatColor.GREEN, 5),
    ROSA("Rosa", ChatColor.LIGHT_PURPLE, 6),
    GRIS("Gris", ChatColor.DARK_GRAY, 7),
    GRIS_CLARO("Gris Claro", ChatColor.GRAY, 8),
    CYAN("Cyan", ChatColor.AQUA, 9),
    MORADO("Morado", ChatColor.DARK_PURPLE, 10),
    AZUL("Azul", ChatColor.DARK_BLUE, 11),
    CAFE("Cafe", ChatColor.DARK_RED, 12),
    VERDE("Verde", ChatColor.GREEN, 13),
    ROJO("Rojo", ChatColor.RED, 14),
    NEGRO("Negro", ChatColor.BLACK, 15);

    private String displayName;
    private ChatColor chatColor;
    private Byte blockId;

    GameColors(String dN, ChatColor cc, int bi) {
        this.displayName = dN;
        this.chatColor = cc;
        this.blockId = (byte)bi;
    }

    public String getDisplayName() {
        return displayName;
    }

    public ChatColor getChatColor() {
        return chatColor;
    }

    public Byte getBlockId() {
        return blockId;
    }

    public static GameColors getFromByte(Byte b) {
        for(GameColors g : values()) {
            if(g.getBlockId() == b){
                return g;
            }
        }
        return null;
    }
}
