package net.gitlab.richthelord.BlockParty.Game.Player;

import net.gitlab.richthelord.BlockParty.Game.Game;
import net.gitlab.richthelord.BlockParty.Main;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.UUID;

public class GamePlayer {

    // PLAYER DATA
    private Player player;
    private String name;
    private UUID uniqueId;

    // GAME (plugin) INFO
    private Game game;

    // PLAYER STATS
    private int points;
    private int wins;
    private int looses;
    private int coins;

    public GamePlayer(Player player) {
        this.player = player;
        this.name = player.getName();
        this.uniqueId = player.getUniqueId();

        game = null;
    }

    public boolean existsInDatabse() {
        return false;
    }

    public void loadStatsFromSQL() {

    }

    public void removeFromServer() {
        if(isInGame()) {
            game.removePlayer(this, false);
        }

        GamePlayerManager.getManager().unregisterPlayer(this);
        if(player.isOnline()) {
            player.kickPlayer("Gracias por jugar.");
        }
    }

    public void addToServer() {

    }

    public Game getGame() {
        return game;
    }

    public String getName() {
        return name;
    }

    public Player getPlayer() {
        return player;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public int getCoins() {
        return coins;
    }

    public int getLooses() {
        return looses;
    }

    public int getPoints() {
        return points;
    }

    public int getWins() {
        return wins;
    }

    public void setCoins(int coins) {
        this.coins = coins;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public boolean isInGame() {
        return game != null;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    public void setLooses(int looses) {
        this.looses = looses;
    }

    public void sendMessage(String message) {
        player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.PREFIX + message));
    }

    public void sendMessage(String... message) {
        for(String str : message) {
            player.sendMessage(ChatColor.translateAlternateColorCodes('&', Main.PREFIX + str));
        }
    }
}
