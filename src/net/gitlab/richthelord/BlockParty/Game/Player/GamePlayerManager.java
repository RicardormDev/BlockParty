package net.gitlab.richthelord.BlockParty.Game.Player;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

public class GamePlayerManager {

    private static GamePlayerManager manager = new GamePlayerManager();
    public static GamePlayerManager getManager() {
        return manager;
    }

    private ArrayList<GamePlayer> players;
    public GamePlayerManager() {
        players = new ArrayList<>();
    }

    public void registerPlayer(GamePlayer gamePlayer) {
        players.add(gamePlayer);
    }

    public void unregisterPlayer(GamePlayer gamePlayer) {
        players.remove(gamePlayer);
    }

    public GamePlayer get(Player player) {
        for(GamePlayer gamePlayer : players) {
            if(gamePlayer.getPlayer() == player) {
                return gamePlayer;
            }
        }
        return null;
    }

    public GamePlayer get(String name) {
        for(GamePlayer gamePlayer : players) {
            if(gamePlayer.getName().equals(name)) {
                return gamePlayer;
            }
        }
        return null;
    }

    public GamePlayer get(UUID uuid) {
        for(GamePlayer gamePlayer : players) {
            if(gamePlayer.getUniqueId() == uuid) {
                return gamePlayer;
            }
        }
        return null;
    }
}
