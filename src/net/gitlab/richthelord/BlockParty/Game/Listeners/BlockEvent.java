package net.gitlab.richthelord.BlockParty.Game.Listeners;

import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayer;
import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayerManager;
import org.bukkit.Material;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public class BlockEvent implements Listener{

    @EventHandler
    public void placeBlockEvent(BlockPlaceEvent e) {
        GamePlayer gamePlayer = GamePlayerManager.getManager().get(e.getPlayer());

        if(gamePlayer != null && gamePlayer.isInGame()) {
            if(e.getBlock().getType() == Material.STAINED_CLAY){
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void breakBlockEvent(BlockBreakEvent e) {
        GamePlayer gamePlayer = GamePlayerManager.getManager().get(e.getPlayer());

        if(gamePlayer != null && gamePlayer.isInGame()) {
            e.setCancelled(true);
        }
    }
}
