package net.gitlab.richthelord.BlockParty.Game.Listeners;

import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayer;
import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayerManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class CommandEvent implements Listener{

    @EventHandler
    public void sendCommandEvent(PlayerCommandPreprocessEvent e) {
        GamePlayer player = GamePlayerManager.getManager().get(e.getPlayer());

        if(e.getMessage().contains("salir") || e.getMessage().contains("leave") || e.getMessage().contains("quit") || e.getMessage().contains("bp") || e.getMessage().contains("blockparty")) {
            return;
        }

        if(player != null && player.isInGame() && !e.getPlayer().isOp()) {
            player.sendMessage("§cNo puedes usar comando en juego.");
            e.setCancelled(true);
        }
    }
}
