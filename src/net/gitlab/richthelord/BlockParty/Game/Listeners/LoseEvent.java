package net.gitlab.richthelord.BlockParty.Game.Listeners;

import net.gitlab.richthelord.BlockParty.Game.Game;
import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayer;
import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayerManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class LoseEvent implements Listener{

    @EventHandler
    public void onMovePlayerEvent(PlayerMoveEvent e) {
        GamePlayer gamePlayer = GamePlayerManager.getManager().get(e.getPlayer());

        if(gamePlayer == null) return;
        if(!gamePlayer.isInGame()) return;
        Game game = gamePlayer.getGame();

        if(e.getFrom().getBlockY() < game.getGameFloor().getPoint1().getBlockY() - 10 && !(game.getSpectators().contains(gamePlayer))) {
            game.removePlayer(gamePlayer, true);
        }
    }

}
