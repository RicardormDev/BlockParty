package net.gitlab.richthelord.BlockParty.Game.Listeners;

import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayer;
import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayerManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class FoodEvent implements Listener{

    @EventHandler
    public void onFoodLoose(FoodLevelChangeEvent e) {
        if(e.getEntity() instanceof Player) {
            GamePlayer gamePlayer = GamePlayerManager.getManager().get((Player) e.getEntity());

            if(gamePlayer != null && gamePlayer.isInGame()) {
                e.setCancelled(true);
            }

        }
    }

}
