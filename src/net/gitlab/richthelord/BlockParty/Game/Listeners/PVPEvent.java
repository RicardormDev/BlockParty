package net.gitlab.richthelord.BlockParty.Game.Listeners;

import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayer;
import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayerManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class PVPEvent implements Listener{

    @EventHandler
    public void hit(EntityDamageByEntityEvent e) {
        if(e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
            GamePlayer gamePlayer1 = GamePlayerManager.getManager().get((Player) e.getEntity());
            GamePlayer gamePlayer2 = GamePlayerManager.getManager().get((Player) e.getDamager());

            if((gamePlayer1 != null && gamePlayer1.isInGame()) || (gamePlayer2 != null && gamePlayer2.isInGame())) {
                e.setCancelled(true);
            }
        }
    }
}
