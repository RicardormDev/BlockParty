package net.gitlab.richthelord.BlockParty.Game.Runnables;

import net.gitlab.richthelord.BlockParty.Game.Game;
import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayer;
import net.gitlab.richthelord.BlockParty.Main;
import net.minecraft.server.v1_8_R3.ChatComponentText;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class GameRunnable {
    private Game game;
    private int iWaiter;
    private double iCounter;
    private int g;

    private BukkitRunnable global;
    private BukkitRunnable t1;
    private BukkitRunnable timer2;
    private BukkitRunnable waiter;

    public GameRunnable(Game game) {
        this.game = game;
        iWaiter = 3;
        iCounter = game.getSpeed();
    }

    public void cancelTimers() {
        if (t1 != null) t1.cancel();
        if (timer2 != null) timer2.cancel();
    }

    public double getDownFloorTime() {
        return iCounter;
    }

    public void startTimers() {
        game.getGameFloor().loadSafe();
        game.giveReferenceItem();
        iCounter = 5;
        BukkitRunnable t2 = new BukkitRunnable() {

            @Override
            public void run() {
                if (iCounter <= 0) {
                    if ((game.getRound() % 5) == 0) {
                        //game.setSpeedLevel(game.getSpeedLevel() + 1);

                        if (!(game.getSpeedLevel() <= 0)) {
                            game.setSpeed(game.getSpeed() - 0.5);
                        }

                    }

                    iCounter = game.getSpeed();
                    game.setRound(game.getRound() + 1);
                    game.getGameFloor().removebBlocks();
                    for(GamePlayer gamePlayer : game.getPlayers()){
                        gamePlayer.getPlayer().playSound(gamePlayer.getPlayer().getLocation(), Sound.ENDERDRAGON_WINGS, 10, 10);
                    }
                    //game.getGameFloor().switchNextToCurrent();

                    waiter = new BukkitRunnable() {
                        @Override
                        public void run() {
                            game.getGameFloor().placeBlocks();
                            startTimers();
                            cancel();
                        }
                    };
                    waiter.runTaskLater(Main.getPlugin(), 60);

                    this.cancel();
                    return;
                }

                // DEBUG: game.broadcastMessage("Suelo cae en: " + iCounter);
                // DEBUG: System.out.println(" RONDA: " + game.getRound());
                // DEBUG: System.out.println(" SPEED: " + game.getSpeed());
                // DEBUG: System.out.println(" SPEED LEVEL: " + game.getSpeedLevel());
                iCounter -= 0.1;
            }

        };
        t2.runTaskTimer(Main.getPlugin(), 0, 2);
        timer2 = t2;
    }

    private BukkitRunnable waiterToPlace;
    private void go() {
        game.getGameFloor().loadSafe();
        game.giveReferenceItem();

        BukkitRunnable broker = new BukkitRunnable() {
            double brokerCounter = 5;
            @Override
            public void run() {

                if (brokerCounter <= 0) {
                    if ((game.getRound() % 5) == 0) {
                        game.setSpeed(game.getSpeed() - 0.5);
                    }

                    brokerCounter = game.getSpeed();
                    game.setRound(game.getRound() + 1);

                    game.getGameFloor().removebBlocks();
                    for(GamePlayer gamePlayer : game.getPlayers()){
                        gamePlayer.getPlayer().playSound(gamePlayer.getPlayer().getLocation(), Sound.ENDERDRAGON_WINGS, 10, 10);
                    }

                    waiterToPlace = new BukkitRunnable() {
                        @Override
                        public void run() {
                            game.getGameFloor().placeBlocks();
                            go();
                            cancel();
                        }
                    };
                    // waiter: Start
                    cancel();
                    return;
                }

                brokerCounter -= 0.1;
            }
        };
    }




              // DEBUG: System.out.println(iWaiter);



    public void globalCounter() {
        global = new BukkitRunnable() {
            @Override
            public void run() {
                g++;
                for(GamePlayer gamePlayer : game.getPlayers()) {
                    sendActionText(gamePlayer.getPlayer(), game.dataTimeAction(gamePlayer.getPlayer()) + " " + (int)iCounter + " " + game.dataTimeAction(gamePlayer.getPlayer()));
                }
            }
        };
        global.runTaskTimer(Main.getPlugin(), 0, 20);
    }

    private void sendActionText(Player player, String message) {
        PacketPlayOutChat packetPlayOutChat = new PacketPlayOutChat(new ChatComponentText(message), (byte) 2);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packetPlayOutChat);
    }

    public void stopAll() {
        global.cancel();
        timer2.cancel();
        waiter.cancel();
    }
}
