package net.gitlab.richthelord.BlockParty.Game.Runnables;

import net.gitlab.richthelord.BlockParty.Game.Game;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;

public class GameCountdown extends BukkitRunnable{

    private Game game;
    private int i;
    private ArrayList<Integer> countingNums;

    public GameCountdown(Game game, int start, int... numbers) {
        this.i = start;
        this.game = game;
        this.countingNums = new ArrayList<>();

        for(int i : numbers){
            countingNums.add(i);
        }
    }

    public int getCount() {
        return i;
    }

    @Override
    public void run() {
        if(i == 0){
            game.broadcastMessage("El juego ha comenzado!");

            game.startGame();
            cancel();
            return;
        }

        if(countingNums.contains(i)){
            game.broadcastMessage("&aEl juego comenzará en &b" + i + " &asegundos.");
        }

        game.getGameScoreboard().update();
        i--;
    }

}
