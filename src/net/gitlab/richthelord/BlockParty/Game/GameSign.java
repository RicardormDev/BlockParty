package net.gitlab.richthelord.BlockParty.Game;

import net.gitlab.richthelord.BlockParty.SettingsManager;
import net.gitlab.richthelord.BlockParty.Utils.RandomString;
import org.bukkit.Location;
import org.bukkit.block.Sign;

public class GameSign {

    private Location location;
    private Sign sign;
    private Game game;

    public GameSign(Location loc, Game game) {
        this.location = loc;
        this.sign = (Sign) loc.getBlock().getState();
        this.game = game;

        game.addGameSign(this);
    }

    public Game getGame() {
        return game;
    }

    public void update(){
        sign.setLine(0, "§f§l[§3§lBlock§6§lParty§f§l]");
        sign.setLine(1, "§6" + game.getGameName());
        sign.setLine(2, "§a" + game.getGameStage().toString());
        sign.setLine(3, "§6" + game.getPlayers().size() + "§3/§6" + game.getMaxPlayers());
        sign.update();
    }

    public void save() {
        String s = new RandomString().nextString();
        SettingsManager.getSigns().set(s + ".game", game.getGameName());
        SettingsManager.getSigns().set(s + ".world", location.getWorld().getName());
        SettingsManager.getSigns().set(s + ".x", location.getBlockX());
        SettingsManager.getSigns().set(s + ".y", location.getBlockY());
        SettingsManager.getSigns().set(s + ".z", location.getBlockZ());
    }
}
