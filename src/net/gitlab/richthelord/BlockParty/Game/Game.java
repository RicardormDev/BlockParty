package net.gitlab.richthelord.BlockParty.Game;

import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayer;
import net.gitlab.richthelord.BlockParty.Game.Runnables.GameCountdown;
import net.gitlab.richthelord.BlockParty.Game.Runnables.GameRunnable;
import net.gitlab.richthelord.BlockParty.Main;
import net.gitlab.richthelord.BlockParty.SettingsManager;
import net.gitlab.richthelord.BlockParty.Utils.FireworksUtil;
import net.gitlab.richthelord.BlockParty.Utils.PacketsUtils;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.*;

public class Game {

    public void setNextSafe(Byte nextSafe) {
        this.nextSafe = nextSafe;
    }

    public Byte getNextSafe() {
        return nextSafe;
    }

    public Location getLobby() {
        return lobby;
    }

    public void setLobby(Location lobby) {
        this.lobby = lobby;
    }

    public enum GameStage {
        WAITING, STARTING, STARTED, ENDING
    }

    private String gameName;
    private UUID uniqueId;

    private Location point1;
    private Location point2;
    private Location lobby;

    private List<GamePlayer> players;
    private List<GamePlayer> spectators;

    private GameStage gameStage;
    private GameRunnable gameTimer;
    private GameFloor gameFloor;
    private GameScoreboard gameScoreboard;
    private GameCountdown gameCountdown; // Just to make it accessible.
    private List<GameSign> gameSigns;

    private int round;
    private int speedLevel;
    private double speed;

    private int speedEffectLevel;

    private final int maxPlayers = 10;
    private final int minPlayersToStart = 4;

    private Byte nextSafe;

    private Random random;

    public Game(String name) {
        this.gameName = name;
        configure();

        this.players = new ArrayList<>();
        this.spectators = new ArrayList<>();

        this.gameStage = GameStage.WAITING;
        this.gameTimer = new GameRunnable(this);
        this.gameFloor = new GameFloor(point1, point2);
        this.gameScoreboard = new GameScoreboard(this);
        this.gameSigns = new ArrayList<>();

        this.round = 1;
        this.speedLevel = 1;
        this.speed = 5;

        this.speedLevel = 2;

        this.random = new Random();
    }

    private void configure() {
        this.uniqueId = UUID.fromString(SettingsManager.getArenas().get("Arenas." + gameName + ".uuid"));
        this.point1 = SettingsManager.getArenas().get("Arenas." + gameName + ".point1");
        this.point2 = SettingsManager.getArenas().get("Arenas." + gameName + ".point2");
        this.lobby  = SettingsManager.getArenas().get("Arenas." + gameName + ".lobby");
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public String getGameName() {
        return gameName;
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public List<GamePlayer> getPlayers() {
        return players;
    }

    public List<GamePlayer> getSpectators() {
        return spectators;
    }

    public GameStage getGameStage() {
        return gameStage;
    }

    public GameRunnable getGameTimer() {
        return gameTimer;
    }

    public GameFloor getGameFloor() {
        return gameFloor;
    }

    public int getRound() {
        return round;
    }

    public double getSpeed() {
        return speed;
    }

    public int getSpeedLevel() {
        return speedLevel;
    }

    public int getSpeedEffectLevel() {
        return speedEffectLevel;
    }

    public GameCountdown getGameCountdown() {
        return gameCountdown;
    }

    public GameScoreboard getGameScoreboard(){
        return gameScoreboard;
    }

    public void setSpeedLevel(int speedLevel) {
        this.speedLevel = speedLevel;
    }

    public void addGameSign(GameSign gameSign) {
        this.gameSigns.add(gameSign);
    }

    public void setRound(int round) {
        this.round = round;
        gameScoreboard.update();
    }

    public void updateSpeed() {
        this.speed = 5 / speedLevel;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public void addPlayer(GamePlayer player) {

        if(player.isInGame()) {
            player.sendMessage("&cYa estas en un juego.");
            return;
        }

        if(gameStage == GameStage.STARTED || gameStage == GameStage.ENDING) {
            player.sendMessage("&cEste juego ya ha comenzado.");
            return;
        }

        if(players.size() >= maxPlayers) {
            player.sendMessage("&cEste juego ya está lleno.");
            return;
        }

        players.add(player);
        player.setGame(this);

        player.getPlayer().getInventory().clear();
        player.getPlayer().setHealth(20);
        player.getPlayer().setFoodLevel(20);
        player.getPlayer().setGameMode(GameMode.SURVIVAL);
        //tpRandomOnArena(player, 1);
       player.getPlayer().teleport(lobby);

       if(gameStage == GameStage.STARTING) {
            tpRandomOnArena(player, 1);
       }

        player.sendMessage("&6Has entrando a un juego!");
        broadcastMessage("&6" + player.getName() + " &3entró al juego! &6" + players.size() + "&3/&610");
        updateSigns();
        //player.getPlayer().setScoreboard(gameScoreboard.create());
        gameScoreboard.update();

        if(players.size() >= minPlayersToStart && gameStage == GameStage.WAITING) {
            startCountdown();
        }
    }

    public void removePlayer(GamePlayer gamePlayer, boolean loose) {
        players.remove(gamePlayer);
        updateSigns();

        if(gameStage == GameStage.STARTED || gameStage == GameStage.ENDING){
            transforToSpectator(gamePlayer);
            if(loose) {
                gamePlayer.sendMessage("&6Perdiste! Ahora puedes espectar.");
                broadcastMessage(ChatColor.GOLD + gamePlayer.getName() + " a tocado fondo!");
                gamePlayer.getPlayer().getWorld().strikeLightningEffect(gamePlayer.getPlayer().getLocation());
            }


            if(players.size() == 1 && gameStage != GameStage.ENDING) {
                endGame();
            }
        } else {
            deletePlayerFromGame(gamePlayer);

            if(players.size() < minPlayersToStart && gameStage == GameStage.STARTING) {
                gameStage = GameStage.WAITING;
                gameCountdown.cancel();
                broadcastMessage("Jugadores insuficientes, esperando...");
            }
        }

    }

    private void transforToSpectator(GamePlayer player) {
        player.getPlayer().setGameMode(GameMode.SPECTATOR);
        spectators.add(player);
        tpRandomOnArena(player, 10);
    }

    private void deletePlayerFromGame(GamePlayer gamePlayer) {
        if(spectators.contains(gamePlayer))
            spectators.remove(gamePlayer);

        if(players.contains(gamePlayer))
            players.remove(gamePlayer);

        gameScoreboard.removeScoreboard(gamePlayer);
        gamePlayer.getPlayer().getInventory().clear();
        gamePlayer.getPlayer().setGameMode(GameMode.ADVENTURE);
        gamePlayer.getPlayer().teleport(new Location(Bukkit.getWorld("BlockParty"), -46, 19, -172, 90, 1));
        gamePlayer.setGame(null);
    }

    public void startCountdown() {
        this.gameStage = GameStage.STARTING;
        updateSigns();
        broadcastMessage(Main.PREFIX + "§6Comenzando juego!", true);

        for(GamePlayer player : players) {
            tpRandomOnArena(player, 1);
            PacketsUtils.sendTitle(player.getPlayer(), 3, 5, 3, "§5Comenzando!", "§amc.volkmc.net");
        }

        this.gameCountdown = new GameCountdown(this, 10, 10, 5, 4, 3, 2, 1);
        this.gameCountdown.runTaskTimer(Main.getPlugin(), 0, 20);
    }

    public void startGame() {
        this.gameStage = GameStage.STARTED;
        updateSigns();

        new BukkitRunnable() {
            @Override
            public void run() {
                gameTimer.startTimers();
                gameTimer.globalCounter();

                for(GamePlayer gamePlayer : players){
                    Player player = gamePlayer.getPlayer();
                    //tpRandomOnArena(gamePlayer, 1);
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 1000000, 1, false), false);
                }

            }
        }.runTaskLater(Main.getPlugin(), 60);
    }

    public void forceStartGame() {
        this.gameStage = GameStage.STARTING;
        updateSigns();
        this.gameCountdown = new GameCountdown(this, 5, 5, 4, 3, 2, 1);
        this.gameCountdown.runTaskTimer(Main.getPlugin(), 0, 20);
    }

    private void endGame() {
        gameStage = GameStage.ENDING;
        this.gameTimer.cancelTimers();
        GamePlayer win = players.get(0);
        broadcastMessage("&6&l" + win.getName() + " &3&lganó el juego!");

        gameTimer.stopAll();
        gameFloor.placeBlocks();

        new BukkitRunnable() {
            int off = 8;
            @Override
            public void run() {

                if(off <= 0) {
                    for(GamePlayer s : spectators){
                        deletePlayerFromGame(s);
                    }
                    deletePlayerFromGame(win);
                    gameStage = GameStage.WAITING;
                    updateSigns();
                    cancel();
                    return;
                }

                FireworksUtil.shootFireworks(win.getPlayer());
                off--;
            }
        }.runTaskTimer(Main.getPlugin(), 0, 20);
    }

    public void broadcastMessage(String msg) {
        for(GamePlayer p : players) {
            p.sendMessage(msg);
        }

        for(GamePlayer p : spectators) {
            p.sendMessage(msg);
        }
    }

    public void broadcastMessage(String msg, boolean sound) {
        for(GamePlayer p : players) {
            p.sendMessage(msg);
            p.getPlayer().playSound(p.getPlayer().getLocation(), Sound.CLICK, 10, 10);
        }

        for(GamePlayer p : spectators) {
            p.sendMessage(msg);
        }
    }

    private void updateSigns() {
        for(GameSign s : gameSigns) {
            s.update();
        }
    }

    @SuppressWarnings("deprecation")
    public void giveReferenceItem() {
        for(GamePlayer gamePlayer : players) {
            Player player = gamePlayer.getPlayer();

            ItemStack item = new ItemStack(Material.STAINED_CLAY, 1, (short)0, gameFloor.getSafe().getBlockId());
            ItemMeta meta = item.getItemMeta();
            meta.setDisplayName(gameFloor.getSafe().getChatColor() + "" + ChatColor.BOLD + gameFloor.getSafe().getDisplayName().toUpperCase());
            item.setItemMeta(meta);

            player.getInventory().setItem(4, item);
        }
    }

    public String dataTimeAction(Player player) {
        String ret = "";
        int per = (int) (((int)gameTimer.getDownFloorTime() * 100) / speed);
        // DEBUG :System.out.println(per);

        switch (per){
            case 100:
                ret = "██████";
                //player.playNote(player.getLocation(), Instrument.PIANO, Note.natural(1, Note.Tone.G));
                break;
            case 80:
            case 85:
            case 88:
            case 75:
                ret = "████";
                //player.playNote(player.getLocation(), Instrument.PIANO, Note.natural(1, Note.Tone.F));
                break;
            case 60:
            case 66:
            case 50:
            case 57:
                ret = "███";
                player.playNote(player.getLocation(), Instrument.BASS_GUITAR, Note.natural(0, Note.Tone.E));
                break;
            case 40:
            case 44:
            case 25:
            case 28:
                ret = "██";
                player.playNote(player.getLocation(), Instrument.BASS_GUITAR, Note.natural(0, Note.Tone.D));
                break;
            case 20:
            case 22:
                ret = "█";
                player.playNote(player.getLocation(), Instrument.BASS_GUITAR, Note.natural(0, Note.Tone.C));
                break;
        }

        return gameFloor.getSafe().getChatColor() + ret + ChatColor.RESET;
    }

    private void tpRandomOnArena(GamePlayer gamePlayer, int transformY) {
        gamePlayer.getPlayer().teleport(gameFloor.getBlockLocations().get(random.nextInt(gameFloor.getBlockLocations().size())).add(0, transformY, 0));
    }
}
