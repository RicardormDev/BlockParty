package net.gitlab.richthelord.BlockParty.Game.Events;

import net.gitlab.richthelord.BlockParty.Game.Game;
import net.gitlab.richthelord.BlockParty.Game.GameManager;
import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayer;
import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayerManager;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class SignClickEvent implements Listener{

    @EventHandler (ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onSignClickEvent(PlayerInteractEvent e) {
        GamePlayer gamePlayer = GamePlayerManager.getManager().get(e.getPlayer());

        if(e.getAction() != Action.RIGHT_CLICK_BLOCK)
            return;

        Block b = e.getClickedBlock();
        if(b.getType() == Material.SIGN || b.getType() == Material.SIGN_POST || b.getType() == Material.WALL_SIGN) {
            Sign sign = (Sign) b.getState();
            if(sign.getLine(0).equals("§f§l[§3§lBlock§6§lParty§f§l]")) {
                String name = ChatColor.stripColor(sign.getLine(1));
                Game game = GameManager.getManager().getGame(name);
                game.addPlayer(gamePlayer);
            }
        }

    }
}

