package net.gitlab.richthelord.BlockParty.Game.Events;

import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayer;
import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinedEvent implements Listener {

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onPlayerJoinEvent(PlayerJoinEvent e) {
        GamePlayer gamePlayer = GamePlayerManager.getManager().get(e.getPlayer());
        if(gamePlayer== null) {
            gamePlayer = new GamePlayer(e.getPlayer());
            GamePlayerManager.getManager().registerPlayer(gamePlayer);
        }

        if(!gamePlayer.isInGame()) {
            gamePlayer.getPlayer().teleport(new Location(Bukkit.getWorld("BlockParty"), -46, 19, -172, 90, 1));
        }
    }

}
