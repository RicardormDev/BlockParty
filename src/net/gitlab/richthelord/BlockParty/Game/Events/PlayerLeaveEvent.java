package net.gitlab.richthelord.BlockParty.Game.Events;

import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayer;
import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayerManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerLeaveEvent implements Listener{

    @EventHandler
    public void onPlayerLeaveEvent(PlayerQuitEvent e) {
        GamePlayer gamePlayer = GamePlayerManager.getManager().get(e.getPlayer());

        if(gamePlayer != null) {
            gamePlayer.removeFromServer();
        }
    }
}
