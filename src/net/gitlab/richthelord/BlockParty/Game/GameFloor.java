package net.gitlab.richthelord.BlockParty.Game;

import com.boydti.fawe.FaweAPI;
import com.boydti.fawe.object.visitor.FastIterator;
import com.boydti.fawe.util.EditSessionBuilder;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.blocks.BaseBlock;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.regions.Region;
import net.gitlab.richthelord.BlockParty.Main;
import net.gitlab.richthelord.BlockParty.Utils.LocationUtils;
import net.minecraft.server.v1_8_R3.BlockPosition;
import net.minecraft.server.v1_8_R3.IBlockData;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameFloor {

    private Location point1;
    private Location point2;

    private Random random;
    private List<Location> blockLocations;
    private int lastModel;
    private GameColors safe;

    /**

     * Esta clase es basicamente el piso del juego
     * de este piso depende practicamente el juego
     * pues de eso se trata, de estar parado en el
     * lugar correcto.
     *
     * <p>En esta clase se procesa todo lo que tiene
     * que ver con el piso que viene siendo el color
     * que tiene que eliminar ahora, guardar sus puntos
     * (donde se encutran los bloques) y también carga las
     * schematics de manera al azar.</p>
     *
     * @author Ricardo Rodríguez
     * @see Location
     * @since 1.0

     **/

    public GameFloor(Location point1, Location point2) {
        this.point1 = point1;
        this.point2 = point2;

        this.random = new Random();
        this.blockLocations = LocationUtils.getLocations(point1, point2);
        this.lastModel = -1;
    }

    /**
     *
     * @return regresa el punto de
     *         pegado y el punto principal Location
     */
    public Location getPoint1() {
        return point1;
    }

    /**
     *
     * @return regresa el punto número 2 del piso.
     */

    public Location getPoint2() {
        return point2;
    }

    public List<Location> getBlockLocations(){
        return blockLocations;
    }

    /**
     *
     * Remove the <i>random</i> selected block.
     *
     */

    public void removebBlocks() {
        //setBlockFast(loc.getWorld(), loc.getBlockX(), loc.getBlockY(), loc.getBlockZ(), 1, (byte) 0);

        //Region region = new CuboidRegion(new Vector(point1.getBlockX(), point1.getBlockY(), point1.getBlockZ()), new Vector(point2.getBlockX(), point2.getBlockY(), point1.getBlockZ()));

        EditSession editSession = new EditSessionBuilder(FaweAPI.getWorld(point1.getWorld().getName())).fastmode(true).build();
        Region region = new CuboidRegion(new Vector(point1.getBlockX(), point1.getBlockY(), point1.getBlockZ()), new Vector(point2.getBlockX(), point2.getBlockY(), point2.getBlockZ()));

        //System.out.println(LocationUtils.serilizeLocation(point1) + " " + LocationUtils.serilizeLocation(point2));

        for( Vector pt : new FastIterator(region, editSession)) {
            BaseBlock block = editSession.getBlock(pt);
            if(block.getData() != safe.getBlockId()) {
                try {
                    editSession.setBlock(pt, new BaseBlock(0));
                } catch (MaxChangedBlocksException e) {
                    e.printStackTrace();
                }
            }
        }

        //editSession.setBlocks(region, new BaseBlock(0));
        editSession.flushQueue();

            /*
            Block block = loc.getBlock();
            if(block.getData() != safe.getBlockId()){
                block.setType(Material.AIR);
            }
            */
    }

    public void placeBlocks() {
        int modelID;
        do {
            modelID = random.nextInt(Main.getFloorfiles().size());
        } while (modelID == lastModel);

        EditSession editSession = new EditSessionBuilder(FaweAPI.getWorld(point1.getWorld().getName())).fastmode(true).build();
        Region region = new CuboidRegion(new Vector(point1.getBlockX(), point1.getBlockY(), point1.getBlockZ()), new Vector(point2.getBlockX(), point2.getBlockY(), point2.getBlockZ()));

        //System.out.println(LocationUtils.serilizeLocation(point1) + " " + LocationUtils.serilizeLocation(point2));

        FastIterator f = new FastIterator(region, editSession);

        int mCurrent = getRandomBlock(1, 10);
        int currentID = random.nextInt(15);

        for( Vector pt : f) {

            if(mCurrent == 0) {
                mCurrent = getRandomBlock(1, 10);
                currentID = random.nextInt(15);
            }

            try {
                editSession.setBlock(pt, new BaseBlock(159, currentID));
                } catch (MaxChangedBlocksException e) {
                    e.printStackTrace();
                }
                mCurrent--;
        }

        editSession.flushQueue();
/*
        //SchmaticUtils.placeSchem(Main.getFloormodels().get(modelID), point1);/*
        try {
            EditSession editSession = ClipboardFormat.SCHEMATIC.load(Main.getFloorfiles().get(modelID))
                    .paste(FaweAPI.getWorld(point1.getWorld().getName()), new Vector(point2.getBlockX(), point2.getBlockY() + 1, point2.getBlockZ()),
                            false, true, (Transform) null
                            );
            editSession.flushQueue();
        } catch (Exception e) {
            e.printStackTrace();
        }
        */
    }

    private int getRandomBlock(int min, int max) {
        return random.nextInt(max-min) + min;
    }

    public void loadSafe() {

        List<Byte> found = new ArrayList<>();
        for(Location loc : blockLocations) {
            Block block = loc.getBlock();
            if(block.getType() == Material.STAINED_CLAY)
                if(!found.contains(block.getData()))
                    found.add(block.getData());
        }
        safe = GameColors.getFromByte(found.get(random.nextInt(found.size())));
       // DEBUG System.out.println(StringUtils.join(found, ','));
    }

    public GameColors getSafe() {
        return safe;
    }

    public void setBlockFast(World world, int x, int y, int z, int blockId, byte data) {
        net.minecraft.server.v1_8_R3.World w = ((CraftWorld) world).getHandle();
        net.minecraft.server.v1_8_R3.Chunk chunk = w.getChunkAt(x >> 4, z >> 4);
        BlockPosition bp = new BlockPosition(x, y, z);
        int combined = blockId + (data << 12);
        IBlockData ibd = net.minecraft.server.v1_8_R3.Block.getByCombinedId(combined);
        chunk.a(bp, ibd);
    }
}