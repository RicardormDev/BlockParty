package net.gitlab.richthelord.BlockParty;

/*
 * Copyright 2018, Ricardo Rodríguez Medina, All rights reserved.
 */

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import net.gitlab.richthelord.BlockParty.Commands.CommandManager;
import net.gitlab.richthelord.BlockParty.Commands.LeaveCommand;
import net.gitlab.richthelord.BlockParty.Game.Events.PlayerJoinedEvent;
import net.gitlab.richthelord.BlockParty.Game.Events.PlayerLeaveEvent;
import net.gitlab.richthelord.BlockParty.Game.Events.SignClickEvent;
import net.gitlab.richthelord.BlockParty.Game.GameManager;
import net.gitlab.richthelord.BlockParty.Game.GameSign;
import net.gitlab.richthelord.BlockParty.Game.Listeners.*;
import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayer;
import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayerManager;
import net.gitlab.richthelord.BlockParty.Utils.SchmaticUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;


@SuppressWarnings("deprecation")
public class Main extends JavaPlugin{

    public static final String PREFIX = "&r&l[&3&lBlock&6&lParty&r&l] &r&l>> ";
    public static final String PREFIX_RAW = " >> ";

    //private static ArrayList<CuboidClipboard> floormodels;
    private static ArrayList<File> floorfiles;

    public void onEnable(){
        getCommand("blockparty").setExecutor(new CommandManager());
        getCommand("leave").setExecutor(new LeaveCommand());

        Bukkit.getServer().getPluginManager().registerEvents(new PlayerJoinedEvent(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new PlayerLeaveEvent(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new SignClickEvent(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new LoseEvent(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new BlockEvent(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new PVPEvent(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new FoodEvent(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new CommandEvent(), this);

        //floormodels = new ArrayList<>();
        floorfiles = new ArrayList<>();

        this.getLogger().info(PREFIX_RAW + "Cargando Modelos...");
        SchmaticUtils.loadModels();
        this.getLogger().info(PREFIX_RAW + "------------------------");

        this.getLogger().info(PREFIX_RAW + "Cargando Arenas...");
        GameManager.getManager().loadArenas();
        this.getLogger().info(PREFIX_RAW + "------------------------");

        this.getLogger().info(PREFIX_RAW + "Cargando jugadores activos...");
        for(Player player : Bukkit.getOnlinePlayers()) {
            GamePlayer gamePlayer = GamePlayerManager.getManager().get(player);
            if(gamePlayer == null) {
                gamePlayer = new GamePlayer(player);
                GamePlayerManager.getManager().registerPlayer(gamePlayer);
            }
        }
        this.getLogger().info(PREFIX_RAW + "------------------------");

        this.getLogger().info(PREFIX_RAW + "Cargando Señales...");
        for(String s : SettingsManager.getSigns().getKeys()) {
            String gameName = SettingsManager.getSigns().get(s+".game");
            String world = SettingsManager.getSigns().get(s+".world");
            int x = SettingsManager.getSigns().get(s+".x");
            int y = SettingsManager.getSigns().get(s+".y");
            int z = SettingsManager.getSigns().get(s+".z");

            Location loc = new Location(Bukkit.getWorld(world), x, y, z);
            GameSign lSign = new GameSign(loc, GameManager.getManager().getGame(gameName));
            lSign.update();
            this.getLogger().info(PREFIX_RAW + "Cargando: " + s);
        }
        this.getLogger().info(PREFIX_RAW + "------------------------");
    }

    public static Plugin getPlugin() {
        return Bukkit.getServer().getPluginManager().getPlugin("BlockParty");
    }

    public static WorldEditPlugin getWorldEdit() {
        return (WorldEditPlugin) Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
    }
/*
    public static ArrayList<CuboidClipboard> getFloormodels() {
        return floormodels;
    }
*/
    public static ArrayList<File> getFloorfiles () {
        return floorfiles;
    }


}
