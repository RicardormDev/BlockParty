package net.gitlab.richthelord.BlockParty;

import com.sk89q.worldedit.*;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.bukkit.selections.Selection;
import com.sk89q.worldedit.schematic.SchematicFormat;
import net.gitlab.richthelord.BlockParty.Game.Game;
import net.gitlab.richthelord.BlockParty.Game.GameFloor;
import net.gitlab.richthelord.BlockParty.Game.GameManager;
import net.gitlab.richthelord.BlockParty.Game.GameSign;
import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayer;
import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayerManager;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.HashSet;

public class CommandsOld implements CommandExecutor {

    GameFloor floor;

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(cmd.getName().equalsIgnoreCase("blockparty") || cmd.getName().equalsIgnoreCase("bp")){

            if(args.length == 0) {

            } else if(args[0].equalsIgnoreCase("help")) {

            } else if(args[0].equalsIgnoreCase("admin")) {
                Player player = (Player) sender;
                if(args.length < 2) {
                   sender.sendMessage(c("&8&l[&3&lVolk&9&lP&6&la&2&lr&c&lt&5&ly&8&l]&c "));
                    return true;
                }

                if(args[1].equalsIgnoreCase("test")) {

                    if(args[2].equalsIgnoreCase("t")) {
                        loadSchem(args[3], player.getLocation());
                    } else if(args[2].equalsIgnoreCase("remove")){
                        if(floor != null);
                        //floor.removebBlocks();
                    } else if(args[2].equalsIgnoreCase("add")) {

                        if(floor == null) {
                            Selection selection = Main.getWorldEdit().getSelection(player);
                            GameFloor f = new GameFloor(selection.getMinimumPoint(), selection.getMaximumPoint());
                            floor = f;
                        }

                        floor.placeBlocks();
                    } else if(args[2].equalsIgnoreCase("testgame")) {
                        player.sendMessage("You started the game");
                        Game game = new Game("testGame");
                        GamePlayer gamePlayer = new GamePlayer(player);
                        game.addPlayer(gamePlayer);
                        game.startCountdown();
                    }
                } else if(args[1].equalsIgnoreCase("addSign")) {
                    if(args[2] == null) {
                        player.sendMessage(c(Main.PREFIX + "&cEspecifica un Juego."));
                        return true;
                    }
                    Game game = GameManager.getManager().getGame(args[2]);

                    if(game == null) {
                        player.sendMessage(Main.PREFIX + "&cEse juego no existe.");
                        return true;
                    }

                    Block blockTarget = player.getTargetBlock((HashSet<Byte>)null, 10);
                    if(blockTarget == null) {
                        player.sendMessage(c(Main.PREFIX + "&cSelecciona un bloque."));
                        return true;
                    }

                    if(!(blockTarget.getState() instanceof Sign)){
                        player.sendMessage(c(Main.PREFIX + "&cNo estas viendo una señal."));
                        return true;
                    }

                    GameSign lSign = new GameSign(blockTarget.getLocation(), game);
                    lSign.update();
                    player.sendMessage(c(Main.PREFIX + "&aSeñal agregada."));
                    lSign.save();
                }
            } else if(args[0].equalsIgnoreCase("salir")) {

                if(!(sender instanceof Player)) return true;

                Player player = (Player) sender;
                GamePlayer gamePlayer = GamePlayerManager.getManager().get(player);

                if(gamePlayer == null) {
                    sender.sendMessage(Main.PREFIX + "§cNo existes."); return true;
                }

                Game game = gamePlayer.getGame();

                if(!gamePlayer.isInGame()) {
                    sender.sendMessage(Main.PREFIX + "§cNo estas en ningun juego.");
                    return true;
                }

                game.removePlayer(gamePlayer, false);
            }
        }

        return true;
    }

    public String c(String msg) {
        return ChatColor.translateAlternateColorCodes('&', msg);
    }

    @SuppressWarnings("deprecation")
    public void loadSchem(String schem, Location loc){
        File file = new File("plugins/BlockParty/schematics/" + schem + ".schematic");
        try{
            Vector v = new Vector(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
            BukkitWorld bw = new BukkitWorld(loc.getWorld());
            //EditSession es = new EditSession(bw, Integer.MAX_VALUE);
            EditSession es = WorldEdit.getInstance().getEditSessionFactory().getEditSession(bw, 0x2b9ac9ff);
            CuboidClipboard cl = SchematicFormat.MCEDIT.load(file);
            cl.place(es, v, false);

        } catch(Exception ex){
            ex.printStackTrace();
        }
    }


}
