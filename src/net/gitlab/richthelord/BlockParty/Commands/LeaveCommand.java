package net.gitlab.richthelord.BlockParty.Commands;

import net.gitlab.richthelord.BlockParty.Game.Game;
import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayer;
import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayerManager;
import net.gitlab.richthelord.BlockParty.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LeaveCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {

        if(!(sender instanceof Player)) {
            sender.sendMessage(Main.PREFIX + "§cEste comando es solo para jugadores.");
            return true;
        }

        Player player = (Player) sender;
        GamePlayer gamePlayer = GamePlayerManager.getManager().get(player);

        if(gamePlayer == null) {
            sender.sendMessage(c(Main.PREFIX + "§cNo existes.")); return true;
        }

        Game game = gamePlayer.getGame();

        if(!gamePlayer.isInGame()) {
            sender.sendMessage(c(Main.PREFIX + "§cNo estas en ningun juego."));
            return true;
        }

        game.removePlayer(gamePlayer, false);
        player.sendMessage(c("&6Saliste del Juego!"));
        game.broadcastMessage(player.getName() + "&6salió del Juego!");
        return true;
    }

    public String c(String msg) {
        return ChatColor.translateAlternateColorCodes('&', Main.PREFIX + msg);
    }
}
