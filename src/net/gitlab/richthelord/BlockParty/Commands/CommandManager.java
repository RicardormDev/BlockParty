package net.gitlab.richthelord.BlockParty.Commands;

import net.gitlab.richthelord.BlockParty.Commands.List.Admin.CommandAddArenaLobby;
import net.gitlab.richthelord.BlockParty.Commands.List.Admin.CommandAddLobbySign;
import net.gitlab.richthelord.BlockParty.Commands.List.Admin.CommandCreateArena;
import net.gitlab.richthelord.BlockParty.Commands.List.Admin.TestCmd;
import net.gitlab.richthelord.BlockParty.Commands.List.User.CommandLeaveGame;
import net.gitlab.richthelord.BlockParty.Main;
import org.apache.commons.lang.StringUtils;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class CommandManager implements CommandExecutor{

    private ArrayList<Cmd> cmds;

    public CommandManager() {
        cmds = new ArrayList<>();

        cmds.add(new CommandCreateArena());
        cmds.add(new CommandAddLobbySign());
        cmds.add(new CommandLeaveGame());
        cmds.add(new TestCmd());
        cmds.add(new CommandAddArenaLobby());
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command cmd, String label, String[] args) {

        if(cmd.getName().equalsIgnoreCase("blockparty")) {
            if(args.length == 0 || (args.length > 1 && (args[0].equalsIgnoreCase("help") || args[0].equalsIgnoreCase("ayuda")))) {
                for(Cmd c : cmds) {
                    CommandInfo info = c.getClass().getAnnotation(CommandInfo.class);
                    if(commandSender.hasPermission(info.permission()) || commandSender.isOp())
                        commandSender.sendMessage("§3" + "/" + label + ((info.forAdmin()) ? " admin" : "") + " [" + StringUtils.join(info.aliases(), " | " ).trim() + "] " + info.usage() + "§6 -> " + info.description());
                }
                return true;
            }


            Cmd c = null;

            for(Cmd ac : cmds) {
                CommandInfo info = ac.getClass().getAnnotation(CommandInfo.class);
                if(args.length > 1 && args[0].equalsIgnoreCase("admin")) {
                    if(info.forAdmin()) {
                        for(String alias : info.aliases()) {
                            if(alias.equalsIgnoreCase(args[1])) {
                                c = ac;
                            }
                        }
                    }
                } else {
                    for(String alias : info.aliases()) {
                        if(alias.equalsIgnoreCase(args[0]) && !info.forAdmin()) {
                            c = ac;
                        }
                    }
                }
            }

            if(c == null) {
                commandSender.sendMessage(c(Main.PREFIX + "§c Ese comando no existe!"));
                return true;
            }

            CommandInfo info = c.getClass().getAnnotation(CommandInfo.class);
            if(info.justPlayer() && !(commandSender instanceof Player)) {
                commandSender.sendMessage(c(Main.PREFIX + "§cEste comando es solo para jugadores."));
                return true;
            }

            List<String> nA = new ArrayList<>();
            for(String str : args)
                nA.add(str);
            nA.remove(0);
            if(info.forAdmin())
                nA.remove(0);
            args = nA.toArray(new String[nA.size()]);

            c.onCommand(commandSender, args);
        }

        return true;
    }

    private String c(String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }
}
