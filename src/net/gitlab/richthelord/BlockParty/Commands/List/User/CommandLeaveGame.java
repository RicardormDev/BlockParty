package net.gitlab.richthelord.BlockParty.Commands.List.User;

import net.gitlab.richthelord.BlockParty.Commands.Cmd;
import net.gitlab.richthelord.BlockParty.Commands.CommandInfo;
import net.gitlab.richthelord.BlockParty.Game.Game;
import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayer;
import net.gitlab.richthelord.BlockParty.Game.Player.GamePlayerManager;
import net.gitlab.richthelord.BlockParty.Main;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandInfo(description = "Salir de Partida", usage = "", aliases = {"salir", "leave"}, permission = "Volk.BlockParty.user", justPlayer = true, forAdmin = false)
public class CommandLeaveGame extends Cmd {


    @Override
    public void onCommand(CommandSender sender, String[] args) {
        Player player = (Player) sender;
        GamePlayer gamePlayer = GamePlayerManager.getManager().get(player);

        if(gamePlayer == null) {
            sender.sendMessage(c(Main.PREFIX + "§cNo existes.")); return;
        }

        Game game = gamePlayer.getGame();

        if(!gamePlayer.isInGame()) {
            sender.sendMessage(c(Main.PREFIX + "§cNo estas en ningun juego."));
            return;
        }

        game.removePlayer(gamePlayer, false);
        player.sendMessage(c("&6Saliste del Juego!"));
        game.broadcastMessage(player.getName() + "&6salió del Juego!");
    }

    public String c(String msg) {
        return ChatColor.translateAlternateColorCodes('&', Main.PREFIX + msg);
    }

}
