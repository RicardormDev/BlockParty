package net.gitlab.richthelord.BlockParty.Commands.List.Admin;

import net.gitlab.richthelord.BlockParty.Commands.Cmd;
import net.gitlab.richthelord.BlockParty.Commands.CommandInfo;
import net.gitlab.richthelord.BlockParty.Game.Game;
import net.gitlab.richthelord.BlockParty.Game.GameManager;
import net.gitlab.richthelord.BlockParty.Main;
import net.gitlab.richthelord.BlockParty.SettingsManager;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

@CommandInfo(description = "Selecciona el lugar de lobby.", usage = "[nombre del arena]", aliases = {"sl", "setlobby"}, permission = "Volk.BlockParty.admin", justPlayer = true, forAdmin = true)
public class CommandAddArenaLobby extends Cmd{

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        Player player = (Player) sender;

        if(args.length < 1){
            player.sendMessage(c(Main.PREFIX + "&cNecesitas escribir un Juego."));
            return;
        }

        if(args[0] == null) {
            player.sendMessage(c(Main.PREFIX + "&cEspecifica un Juego."));
            return;
        }
        Game game = GameManager.getManager().getGame(args[0]);

        if(game == null) {
            player.sendMessage(c(Main.PREFIX + "&cEse juego no existe."));
            return;
        }
        player.sendMessage(c(Main.PREFIX + "&aLobby Seleccionado."));
        SettingsManager.getArenas().set("Arenas." + game.getGameName() + ".lobby", player.getLocation());
    }

    public String c(String msg) {
        return ChatColor.translateAlternateColorCodes('&', msg);
    }
}
