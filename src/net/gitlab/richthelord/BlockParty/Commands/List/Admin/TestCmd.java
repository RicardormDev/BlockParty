package net.gitlab.richthelord.BlockParty.Commands.List.Admin;

import com.boydti.fawe.FaweAPI;
import com.boydti.fawe.object.FawePlayer;
import com.boydti.fawe.object.collection.BlockVectorSet;
import com.boydti.fawe.object.schematic.Schematic;
import com.boydti.fawe.object.visitor.FastIterator;
import com.boydti.fawe.util.EditSessionBuilder;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.blocks.BaseBlock;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.extent.clipboard.io.ClipboardFormat;
import com.sk89q.worldedit.regions.CuboidRegion;
import net.gitlab.richthelord.BlockParty.Commands.Cmd;
import net.gitlab.richthelord.BlockParty.Commands.CommandInfo;
import net.gitlab.richthelord.BlockParty.Utils.LocationUtils;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import javax.xml.validation.Schema;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@CommandInfo(description = "Crea una arena", usage = "", aliases = { "test" }, permission = "Volk.BlockParty.admin", justPlayer = true, forAdmin = true)
public class TestCmd extends Cmd {

    boolean a;

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        Player player = (Player) sender;
/*
        switch (Integer.parseInt(args[3])) {
            case 1:
                player.playNote(player.getLocation(), Instrument.valueOf(args[0]),
                        Note.flat(Integer.parseInt(args[1]), Note.Tone.valueOf(args[2])));
                break;
            case 2:
                player.playNote(player.getLocation(), Instrument.valueOf(args[0]),
                        Note.sharp(Integer.parseInt(args[1]), Note.Tone.valueOf(args[2])));
                break;
            case 3:
                player.playNote(player.getLocation(), Instrument.valueOf(args[0]),
                        Note.natural(Integer.parseInt(args[1]), Note.Tone.valueOf(args[2])));
                break;
            case 4:
                    player.playSound(player.getLocation(), Sound.valueOf(args[0]), 10, 10);
                break;
        }*/

/*
        EditSession eS = new EditSessionBuilder(FaweAPI.getWorld(player.getWorld().getName())).fastmode(true).build();
        FawePlayer fp = FawePlayer.wrap(player);

        List<SaveBlock> toSave = new ArrayList<>();

        File file = new File("plugins/BlockParty/blocks.bp");

        if(file.exists()) {

            try {
                FileInputStream in = new FileInputStream(file);
                ObjectInputStream oin = new ObjectInputStream(in);

                toSave = (List<SaveBlock>) oin.readObject();

                in.close();
                oin.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            FastIterator f = new FastIterator(fp.getSelection(), eS);

            int indexX = 0;
            int indexY = 0;
            for(Vector v : f) {

                if((indexX % 6) == 0) indexY++;

                if(indexX >= toSave.size()) {
                    break;
                }

                try {
                    SaveBlock g = getFrom(indexX, indexY, toSave);
                    eS.setBlock(v, new BaseBlock(g.ID, g.data));
                    System.out.println(g.x + " " +  g.y);
                } catch (MaxChangedBlocksException e) {
                    e.printStackTrace();
                }

                indexX++;
            }

            eS.flushQueue();
            return;
        }

        int x = 0;
        int y = 0;
        for(Location locs : LocationUtils.getLocations(
                BukkitUtil.toLocation(player.getWorld(), fp.getSelection().getMinimumPoint()),
                BukkitUtil.toLocation(player.getWorld(), fp.getSelection().getMaximumPoint())
        )) {

            if((x % 6) == 0) y++;

            SaveBlock saveLocation = new SaveBlock();

            saveLocation.ID = locs.getBlock().getTypeId();
            saveLocation.data = locs.getBlock().getData();
            saveLocation.x = x;
            saveLocation.y = y;

            x++;

            toSave.add(saveLocation);
        }


        try {
            FileOutputStream fileOutputStream = new FileOutputStream("plugins/BlockParty/blocks.bp");
            ObjectOutputStream out = new ObjectOutputStream(fileOutputStream);
            out.writeObject(toSave);
            out.close();
            fileOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //eS.setBlocks(fp.getSelection(), new BaseBlock(0));


/*
if(true) return;

        File file = new File("plugins/BlockParty/test.bp");

        if(file.exists()) {

            try {
                FileInputStream in = new FileInputStream(file);
                ObjectInputStream oin = new ObjectInputStream(in);
                String data = (String) oin.readObject();

                sender.sendMessage("§c" + data);

                in.close();
                oin.close();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            return;
        }

        try {
            FileOutputStream fileOut = new FileOutputStream("plugins/BlockParty/test.bp");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject("String");
            out.close();
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

/*
        if(a) {
            EditSession editSession = new EditSessionBuilder(FaweAPI.getWorld(player.getWorld().getName())).fastmode(true).build();
            Region region = new CuboidRegion(new Vector(0, 64, 0), new Vector(100, 64, 100));
            editSession.setBlocks(region, new BaseBlock(0));
            editSession.flushQueue();
            a = false;
            return;
        }
        EditSession editSession = new EditSessionBuilder(FaweAPI.getWorld(player.getWorld().getName())).fastmode(true).build();
        Region region = new CuboidRegion(new Vector(0, 64, 0), new Vector(100, 64, 100));
        editSession.setBlocks(region, new BaseBlock(1));
        editSession.flushQueue();

        a = true;

/*
        Set<Vector> positions = new HashSet<>();
        positions = new BlockVectorSet();


    }
*/

        if(args[0].equalsIgnoreCase("save")) {

            File file = new File("plugins/BlockParty/schematics/test.schematic");
            FawePlayer fp = FawePlayer.wrap(player);

            CuboidRegion region = new CuboidRegion(new BukkitWorld(player.getWorld()), fp.getSelection().getMaximumPoint(), fp.getSelection().getMinimumPoint());
            Schematic sc = new Schematic(region);
            try {
                sc.save(file, ClipboardFormat.SCHEMATIC);
            } catch (IOException e) {
                e.printStackTrace();
            }

            return;
        }


        EditSession sb = null;
        try {
            sb = ClipboardFormat.SCHEMATIC.load(new File("plugins/BlockParty/schematics/" + args[0] + ".schematic")).paste(BukkitUtil.getLocalWorld(player.getWorld()), BukkitUtil.toVector(player.getLocation()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        sb.flushQueue();


    }

    public SaveBlock getFrom(int x, int y, List<SaveBlock> saveBlocks) {
        for(SaveBlock sb : saveBlocks) {
            if(sb.x == x && sb.y == y) {
                return sb;
            }
        }
        return null;
    }
}
