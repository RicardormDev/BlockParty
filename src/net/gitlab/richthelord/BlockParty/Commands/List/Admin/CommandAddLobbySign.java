package net.gitlab.richthelord.BlockParty.Commands.List.Admin;

import net.gitlab.richthelord.BlockParty.Commands.Cmd;
import net.gitlab.richthelord.BlockParty.Commands.CommandInfo;
import net.gitlab.richthelord.BlockParty.Game.Game;
import net.gitlab.richthelord.BlockParty.Game.GameManager;
import net.gitlab.richthelord.BlockParty.Game.GameSign;
import net.gitlab.richthelord.BlockParty.Main;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.HashSet;

@CommandInfo(description = "Agrega un lobby sign", usage = "[nombre del arena]", aliases = {"als", "addlobbysign"}, permission = "Volk.BlockParty.admin", justPlayer = true, forAdmin = true)
public class CommandAddLobbySign extends Cmd {
    @Override
    public void onCommand(CommandSender sender, String[] args) {
        Player player = (Player) sender;

        if(args.length < 1){
            player.sendMessage(Main.PREFIX + "&cNecesitas escribir un Juego.");
            return;
        }

        if(args[0] == null) {
            player.sendMessage(c(Main.PREFIX + "&cEspecifica un Juego."));
            return;
        }
        Game game = GameManager.getManager().getGame(args[0]);

        if(game == null) {
            player.sendMessage(c(Main.PREFIX + "&cEse juego no existe."));
            return;
        }

        Block blockTarget = player.getTargetBlock((HashSet<Byte>)null, 10);
        if(blockTarget == null) {
            player.sendMessage(c(Main.PREFIX + "&cSelecciona un bloque."));
            return;
        }

        if(!(blockTarget.getState() instanceof Sign)){
            player.sendMessage(c(Main.PREFIX + "&cNo estas viendo una señal."));
            return;
        }

        GameSign lSign = new GameSign(blockTarget.getLocation(), game);
        lSign.update();
        player.sendMessage(c(Main.PREFIX + "&aSeñal agregada."));
        lSign.save();
    }

    public String c(String msg) {
        return ChatColor.translateAlternateColorCodes('&', msg);
    }
}
