package net.gitlab.richthelord.BlockParty.Commands.List.Admin;

import com.sk89q.worldedit.bukkit.selections.Selection;
import net.gitlab.richthelord.BlockParty.Commands.Cmd;
import net.gitlab.richthelord.BlockParty.Commands.CommandInfo;
import net.gitlab.richthelord.BlockParty.Game.Game;
import net.gitlab.richthelord.BlockParty.Game.GameManager;
import net.gitlab.richthelord.BlockParty.Main;
import net.gitlab.richthelord.BlockParty.SettingsManager;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.UUID;

@CommandInfo(description = "Crea una arena", usage = "[nombre del arena]", aliases = { "createarena", "ca" }, permission = "Volk.BlockParty.admin", justPlayer = true, forAdmin = true)
public class CommandCreateArena extends Cmd{

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        Player player = (Player) sender;

        if(args.length < 1) {
            player.sendMessage(c("&cEscribe el nombre de una Arena."));
            return;
        }

        Game search = GameManager.getManager().getGame(args[0]);

        if(search != null) {
            player.sendMessage(c("&cEsa arena ya existe."));
            return;
        }

        Selection selection = Main.getWorldEdit().getSelection(player);

        if(selection == null) {
            player.sendMessage(c("&cNecesitas crear una selección."));
            return;
        }

        String arenaName = args[0];
        UUID uuid = UUID.randomUUID();

        SettingsManager.getArenas().set("Arenas." + arenaName + ".uuid", uuid.toString());
        SettingsManager.getArenas().set("Arenas." + arenaName + ".point1", selection.getMinimumPoint());
        SettingsManager.getArenas().set("Arenas." + arenaName + ".point2", selection.getMaximumPoint());

        Game game = new Game(arenaName);
        GameManager.getManager().registerGame(game);

        player.sendMessage(c("Game: " + arenaName + " fue creada y activada!"));
        player.sendMessage(c("Info: "));
        player.sendMessage(c("Mundo: " + selection.getWorld().getName()));
        player.sendMessage(c("UUID: " + uuid.toString()));
        player.sendMessage(c("Más información en BlockParty/Arenas.yml"));
        player.sendMessage(c("§c§lNo olvides colocar el lobby :P"));
     // -----------------------------------------------------------------------> Logger
        System.out.println(c("Game: " + arenaName + " fue creada y activada!"));
        System.out.println(c("Info: "));
        System.out.println(c("Mundo: " + selection.getWorld().getName()));
        System.out.println(c("UUID: " + uuid.toString()));
        System.out.println(c("Jugador: " + player.getName()));
        System.out.println(c("Más información en BlockParty/Arenas.yml"));

    }

    public String c(String msg) {
        return ChatColor.translateAlternateColorCodes('&', Main.PREFIX + msg);
    }

}
