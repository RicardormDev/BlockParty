package net.gitlab.richthelord.BlockParty.Commands;

import org.bukkit.command.CommandSender;

public abstract class Cmd {

    public abstract void onCommand(CommandSender sender, String[] args);

}
